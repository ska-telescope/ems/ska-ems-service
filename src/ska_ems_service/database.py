"""
Module to establish a connection to the EMS database using PostgreSQL.
"""

import logging
import os

import psycopg2

POSTGRESS_HOST = os.getenv("POSTGRESS_HOST")
POSTGRESS_PORT = os.getenv("POSTGRESS_PORT")
POSTGRESS_DATABASE = os.getenv("POSTGRESS_DATABASE")
POSTGRESS_USERNAME = os.getenv("POSTGRESS_USERNAME")
POSTGRESS_PASSWORD = os.getenv("POSTGRESS_PASSWORD")


def create_ems_db_connection():
    """
    Create a connection to the EMS database.

    Returns:
        psycopg2 connection object: A connection to the PostgreSQL database if
        successful, None otherwise.
    """
    try:
        conn = psycopg2.connect(
            host=POSTGRESS_HOST,
            port=POSTGRESS_PORT,
            dbname=POSTGRESS_DATABASE,
            user=POSTGRESS_USERNAME,
            password=POSTGRESS_PASSWORD,
        )
        return conn
    except psycopg2.Error as e:
        logging.error({"An error creating a connection to postgress": str(e)})
        return None
