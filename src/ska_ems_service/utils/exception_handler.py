"""
exception_.py

This module provides methods to handle request exceptions.
"""

import logging
from functools import wraps

import requests


def handle_request_exceptions(func):
    """
    Wrap a function in a try-except block to handle request exceptions.

    Parameters:
        func (function): The function to be wrapped by the decorator.

    Returns:
        function: The wrapped function with exception handling.
    """

    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except requests.exceptions.Timeout as e:
            logging.error("Request timed out: %s", e)
        except requests.exceptions.ConnectionError as e:
            logging.error("Connection error occurred: %s", e)
        except requests.exceptions.HTTPError as e:
            logging.error("HTTP error occurred: %s", e)
        except requests.exceptions.RequestException as e:
            logging.error("Request exception occurred: %s", e)
        return None

    return wrapper


def handle_response(
    response,
    messages=None,
    identifiers=None,
    status_code=200,
    return_json=False,
):
    """
    Handle request responses.

    Parameters:
        - response: The response object.
        - messages: A dictionary containing optional 'success' and 'error'
          messages.
        - identifiers: A dictionary containing optional identifiers such
          as asset_id, field_id, etc.
        - status_code: The expected success status code (default is 200).
        - return_json: Whether to return the response JSON (default is False).
    """
    identifiers = identifiers or {}
    messages = messages or {}

    def log_success_message():
        if "success" in messages:
            params = []
            if "value_id" in identifiers:
                params.append(identifiers["value_id"])
            if "field_id" in identifiers:
                params.append(identifiers["field_id"])
            if "asset_id" in identifiers:
                params.append(identifiers["asset_id"])
            if "location_id" in identifiers:
                params.append(identifiers["location_id"])
            if "task_id" in identifiers:
                params.append(identifiers["task_id"])
            if "name" in identifiers:
                params.append(identifiers["name"])

            if params:
                logging.info(messages["success"], *params)
            else:
                logging.info(messages["success"])

    if response.status_code == status_code:
        if return_json:
            try:
                json_response = response.json()
                log_success_message()
                return json_response
            except ValueError as e:
                logging.error("Failed to parse response as JSON (%s)", e)
                return None
        else:
            log_success_message()
            return response

    if response.status_code == 401:
        logging.error("401 Unauthorized Error! Check your credentials.")
        return None

    error_message = messages.get("error", "Unexpected response.")
    logging.error("%s Status code: %s", error_message, response.status_code)
    return None
