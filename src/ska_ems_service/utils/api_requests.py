"""
This module provides a utility function to make HTTP requests.
"""

import os
from dataclasses import dataclass
from typing import Any, Optional

import requests

EMS_GATEWAY_TOKEN = os.getenv("EMS_GATEWAY_TOKEN")
EMS_GATEWAY_URL = os.getenv("EMS_GATEWAY_URL")


@dataclass
class ApiRequest:
    """
    Dataclass object for an API request.
    """

    # pylint: disable=too-many-instance-attributes
    api_url: str
    headers: Optional[dict[str, str]] = None
    bearer_token: Optional[str] = None
    payload: Optional[str] = None
    params: Optional[dict[str, int | Any]] = None
    method: str = "POST"
    timeout: int = 100

    def make_request(self):
        """
        Make an HTTP request.

        Returns:
        - Response: The HTTP response object.

        Raises:
        - ValueError: If an unsupported HTTP method is provided.
        """
        method = self.method.upper()

        if method == "GET":
            return requests.get(
                self.api_url,
                headers=self.headers,
                params=self.params,
                timeout=self.timeout,
            )
        if method == "POST":
            return requests.post(
                self.api_url,
                headers=self.headers,
                json=self.payload,
                timeout=self.timeout,
            )
        if method == "PUT":
            return requests.put(
                self.api_url,
                headers=self.headers,
                data=self.payload,
                timeout=self.timeout,
            )
        if method == "DELETE":
            return requests.delete(
                self.api_url,
                headers=self.headers,
                timeout=self.timeout,
            )
        if method == "PATCH":
            return requests.patch(
                self.api_url,
                headers=self.headers,
                data=self.payload,
                timeout=self.timeout,
            )

        raise ValueError(f"Unsupported HTTP method: {method}")


def get_request(api_url, headers, params):
    """
    Generic EMS Gateway Get request
    """
    return ApiRequest(
        api_url=api_url,
        headers=headers,
        params=params,
        method="GET",
    )


def post_request(api_url, headers, payload):
    """
    Generic EMS Gateway Get request
    """
    return ApiRequest(
        api_url=api_url,
        headers=headers,
        payload=payload,
        method="POST",
    )


def patch_request(api_url, payload):
    """
    Generic EMS Gateway Patch request
    """
    headers = {"Content-Type": "application/json"}

    return ApiRequest(
        api_url=api_url,
        headers=headers,
        payload=payload,
        method="PATCH",
    )


def put_request(api_url, payload):
    """
    Generic EMS Gateway Put request
    """
    headers = {"Content-Type": "application/json"}

    return ApiRequest(
        api_url=api_url,
        headers=headers,
        payload=payload,
        method="PUT",
    )


def delete_request(api_url):
    """
    Generic EMS Gateway Delete request
    """
    headers = {"Content-Type": "application/json"}

    return ApiRequest(
        api_url=api_url,
        headers=headers,
        method="DELETE",
    )
