"""
This module interacts with JIRA.
"""

# pylint: disable=R0801
import os

from ska_ems_service.utils.api_requests import get_request
from ska_ems_service.utils.exception_handler import (
    handle_request_exceptions,
    handle_response,
)

EMS_GATEWAY_TOKEN = os.getenv("EMS_GATEWAY_TOKEN")
EMS_GATEWAY_URL = os.getenv("EMS_GATEWAY_URL")
STFC_TOKEN = os.getenv("STFC_TOKEN")


@handle_request_exceptions
def get_data_modules():
    """
    Retrieve the data modules from the EMS API Gateway.
    """
    api_url = f"{EMS_GATEWAY_URL}/data-modules"
    headers = {
        "Authorization": f"Bearer {EMS_GATEWAY_TOKEN}",
        "Cookie": STFC_TOKEN,
    }
    params = {}

    request = get_request(api_url, headers, params)
    response = request.make_request()

    messages = {"success": "Successfully retrieved data modules"}

    return handle_response(
        response,
        messages,
        return_json=True,
    )
