"""
This module manages Technical Documentations data synchronization between the
EMS database and EMS API Gateway.
"""

import logging

from ska_ems_service.csdb.api_interface import get_data_modules
from ska_ems_service.database import create_ems_db_connection


def import_data_modules():
    """
    Import data module data into the EMS database.
    """
    data_modules = get_data_modules()

    sql_insert_query = """
        INSERT INTO csdb_data_modules (id, data_module_code, part_number,
        description, url) VALUES (%s, %s, %s, %s, %s);
    """

    with create_ems_db_connection() as connection:
        with connection.cursor() as cursor:
            cursor.execute("DELETE FROM csdb_data_modules")

            if data_modules:
                data = [
                    (
                        data_module["confluence_id"],
                        data_module["data_module_code"],
                        data_module["part_number"],
                        data_module["name"],
                        data_module["url"],
                    )
                    for data_module in data_modules
                ]

                cursor.executemany(sql_insert_query, data)
                logging.info(
                    "Successfully imported %s data modules", len(data_modules)
                )
            else:
                logging.info("No data modules found to import")

        connection.commit()
