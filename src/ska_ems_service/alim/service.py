"""
This module manages configuration controlled data synchronization between the
EMS database and EMS API Gateway.
"""

import logging

import pandas as pd

from ska_ems_service.alim.api_interface import get_product_breakdown_structure
from ska_ems_service.database import create_ems_db_connection


def import_product_structure():
    """
    Import product structure data into the EMS database by deleting existing
    configuration control items and inserting new ones.
    """
    # Get ALIM product breakdown structure
    csv_buffer = get_product_breakdown_structure()

    df = pd.read_csv(csv_buffer)

    sql_insert_query = """
        INSERT INTO configuration_control_items (id, item_number, version,
        description, level, parent_id, config_item, seq_number, uom, status,
        quantity_per, item_class, software, serialised, under_change,
        main_equipment)
        VALUES
        (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);
    """

    with create_ems_db_connection() as connection:
        with connection.cursor() as cursor:
            cursor.execute("DELETE FROM configuration_control_items")

            data = [
                (
                    row["id"],
                    row["item_number"],
                    row["version"],
                    row["description"],
                    row["level"],
                    None if pd.isna(row["parent_id"]) else row["parent_id"],
                    row["config_item"],
                    row["seq_number"],
                    row["uom"],
                    row["status"],
                    row["quantity_per"],
                    row["item_class"],
                    row["software"],
                    row["serialised"],
                    row["under_change"],
                    row["main_equipment"],
                )
                for _, row in df.iterrows()
            ]

            cursor.executemany(sql_insert_query, data)

        connection.commit()
        logging.info("Successfully imported product breakdown structure")
