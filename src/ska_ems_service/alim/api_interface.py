"""
This module interacts with ALIM.
"""

# pylint: disable=R0801
import os
from io import StringIO

from ska_ems_service.utils.api_requests import get_request
from ska_ems_service.utils.exception_handler import (
    handle_request_exceptions,
    handle_response,
)

EMS_GATEWAY_TOKEN = os.getenv("EMS_GATEWAY_TOKEN")
EMS_GATEWAY_URL = "https://ems-api.internal.skao.int"
# EMS_GATEWAY_URL = os.getenv("EMS_GATEWAY_URL")


@handle_request_exceptions
def get_product_breakdown_structure():
    """
    Retrieve the product breakdown structure from the EMS API Gateway.
    """
    api_url = f"{EMS_GATEWAY_URL}/pbs"
    headers = {"Authorization": f"Bearer {EMS_GATEWAY_TOKEN}"}
    params = {}

    request = get_request(api_url, headers, params)
    response = request.make_request()

    messages = {
        "success": "Successfully retrieved product breakdown structure"
    }

    res = handle_response(
        response,
        messages,
        return_json=False,
    )

    csv_data = res.content.decode("utf-8")
    csv_buffer = StringIO(csv_data)
    return csv_buffer
