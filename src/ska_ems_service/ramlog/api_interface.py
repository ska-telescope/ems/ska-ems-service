"""
This module interacts with RamLog.
"""

import os

from ska_ems_service.utils.api_requests import get_request
from ska_ems_service.utils.exception_handler import (
    handle_request_exceptions,
    handle_response,
)

RAMLOG_FDE_TOKEN = os.getenv("RAMLOG_FDE_TOKEN")
RAMLOG_FDE_URL = os.getenv("RAMLOG_FDE_URL")

EMS_GATEWAY_TOKEN = os.getenv("EMS_GATEWAY_TOKEN")
EMS_GATEWAY_URL = "https://ems-api.internal.skao.int"


@handle_request_exceptions
def get_product_breakdown_structure():
    """
    Retrieve the product breakdown structure from the EMS API Gateway.
    """
    api_url = f"{EMS_GATEWAY_URL}/lsa/pbs"
    headers = {"Authorization": f"Bearer {EMS_GATEWAY_TOKEN}"}
    params = {}

    request = get_request(api_url, headers, params)
    response = request.make_request()

    messages = {
        "success": "Successfully retrieved LSA product breakdown structure"
    }

    return handle_response(
        response,
        messages,
        return_json=True,
    )


@handle_request_exceptions
def get_systems():
    """
    Retrieve the product breakdown structure from the EMS API Gateway.
    """
    api_url = f"{RAMLOG_FDE_URL}/api/System"
    headers = {"Authorization": f"Bearer {RAMLOG_FDE_TOKEN}"}
    params = {}

    request = get_request(api_url, headers, params)
    response = request.make_request()

    messages = {"success": "Successfully retrieved systems"}

    return handle_response(
        response,
        messages,
        return_json=True,
    )


@handle_request_exceptions
def get_system_slots(system_id):
    """
    Retrieve the product breakdown structure from the EMS API Gateway.
    """
    api_url = f"{RAMLOG_FDE_URL}/api/System/{system_id}/Slots"
    headers = {"Authorization": f"Bearer {RAMLOG_FDE_TOKEN}"}
    params = {}

    request = get_request(api_url, headers, params)
    response = request.make_request()

    messages = {"success": "Successfully retrieved systems slots"}

    return handle_response(
        response,
        messages,
        return_json=True,
    )


@handle_request_exceptions
def get_item_types():
    """
    Retrieve the product breakdown structure from the EMS API Gateway.
    """
    api_url = f"{RAMLOG_FDE_URL}/api/ItemType"
    headers = {"Authorization": f"Bearer {RAMLOG_FDE_TOKEN}"}
    params = {}

    request = get_request(api_url, headers, params)
    response = request.make_request()

    messages = {"success": "Successfully retrieved item types"}

    return handle_response(
        response,
        messages,
        return_json=True,
    )


@handle_request_exceptions
def get_sn_items():
    """
    Retrieve the product breakdown structure from the EMS API Gateway.
    """
    api_url = f"{RAMLOG_FDE_URL}/api/SNItem"
    headers = {"Authorization": f"Bearer {RAMLOG_FDE_TOKEN}"}
    params = {}

    request = get_request(api_url, headers, params)
    response = request.make_request()

    messages = {"success": "Successfully retrieved serialised items"}

    return handle_response(
        response,
        messages,
        return_json=True,
    )


@handle_request_exceptions
def get_system_type(system_type_id):
    """
    Retrieve the product breakdown structure from the EMS API Gateway.
    """
    api_url = f"{RAMLOG_FDE_URL}/api/SystemType/{system_type_id}/Structure"
    headers = {"Authorization": f"Bearer {RAMLOG_FDE_TOKEN}"}
    params = {}

    request = get_request(api_url, headers, params)
    response = request.make_request()

    messages = {"success": "Successfully retrieved system type"}

    return handle_response(
        response,
        messages,
        return_json=True,
    )


@handle_request_exceptions
def get_work_orders():
    """
    Retrieve the product breakdown structure from the EMS API Gateway.
    """
    api_url = f"{RAMLOG_FDE_URL}/api/JobCard"
    headers = {"Authorization": f"Bearer {RAMLOG_FDE_TOKEN}"}
    params = {}

    request = get_request(api_url, headers, params)
    response = request.make_request()

    messages = {"success": "Successfully retrieved work orders"}

    return handle_response(
        response,
        messages,
        return_json=True,
    )
