"""
This module manages RamLog Failure Data Edition data synchronization between
the EMS database and EMS API Gateway.
"""

import logging

import pandas as pd

from ska_ems_service.database import create_ems_db_connection
from ska_ems_service.ramlog.api_interface import (
    get_item_types,
    get_product_breakdown_structure,
    get_system_slots,
    get_system_type,
    get_systems,
)


def import_fde_product_breakdown_structures():
    """
    Import data module data into the EMS database.
    """
    # pylint: disable=R0914
    systems = get_systems()

    if not systems:
        return

    # Get item type information
    item_types = get_item_types()
    item_types_df = pd.DataFrame(item_types)

    # Get system type information
    for system in systems:
        system_types = []
        system_id = system.get("SystemID")
        system_type_id = system.get("SystemTypeID")
        if system_type_id == 94 and system_id == 55:
            system_type = get_system_type(system_type_id)
            system_types.extend(system_type)

            slots = get_system_slots(system_id)
            slots_df = pd.DataFrame(slots)

            # Get parent_SystemTypeRamLogLCNID
            df = pd.DataFrame(system_types)
            df["parent_LCN"] = df["LCN"].apply(
                lambda x: x[:-3] if len(x) > 3 else ""
            )
            lcn_to_id = df.set_index("LCN")["SystemTypeRamLogLCNID"].to_dict()
            df["parent_SystemTypeRamLogLCNID"] = df["parent_LCN"].map(
                lcn_to_id
            )
            df["parent_SystemTypeRamLogLCNID"] = (
                df["parent_SystemTypeRamLogLCNID"].fillna(0).astype(int)
            )

            # Add Part Number and IsSerialized to dataframe
            df = df.merge(
                item_types_df[["ItemTypeID", "RefNumber", "IsSerialized"]],
                on="ItemTypeID",
                how="left",
            )

            df = df.merge(
                slots_df[
                    [
                        "ChildPositionID",
                        "ChildSystemTypeRamLogLCNID",
                        "SerialNumber",
                    ]
                ],
                left_on=["PositionID", "SystemTypeRamLogLCNID"],
                right_on=["ChildPositionID", "ChildSystemTypeRamLogLCNID"],
                how="left",
            )

            # Replace NaN with None
            df = df.where(pd.notna(df), None)

            # Import data
            data_to_insert = [
                (
                    -1 * row["SystemTypeRamLogLCNID"],
                    row["LCNName"],
                    -1 * row["parent_SystemTypeRamLogLCNID"],
                    16660,  # Location_id
                    row["RefNumber"],
                    row["SerialNumber"],
                    "RAMLOG",
                    -1 * row["SystemTypeRamLogLCNID"],
                )
                for index, row in df.iterrows()
            ]

            sql_insert_query = """
                INSERT INTO cmms_assets
                (
                    id,name,
                    parent_id,
                    location_id,
                    part_number,
                    serial_number,
                    datasource,
                    datasource_id
                )
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s);
            """

            with create_ems_db_connection() as connection:
                with connection.cursor() as cursor:
                    cursor.execute(
                        "DELETE FROM cmms_assets WHERE datasource='RAMLOG'"
                    )

                    cursor.executemany(sql_insert_query, data_to_insert)

                connection.commit()
                logging.info("Successfully imported ramlog")


def import_lsa_product_breakdown_structures():
    """
    Import RamLog LSA product breakdown structure into the EMS database by
    deleting existing structures and inserting new ones.
    """
    pbs = get_product_breakdown_structure()

    sql_insert_query = """
        INSERT INTO lsa_pbs (lcn, name, part_number)
        VALUES (%s, %s, %s);
    """

    with create_ems_db_connection() as connection:
        with connection.cursor() as cursor:
            cursor.execute("DELETE FROM lsa_pbs")

            if pbs:
                data = [
                    (lcn["lcn"], lcn["lcn_name"], lcn["part_number"])
                    for lcn in pbs
                ]

                cursor.executemany(sql_insert_query, data)
                logging.info(
                    "Successfully imported LSA product breakdown structure"
                )
            else:
                logging.info(
                    "No LSA product breakdown structure found to import"
                )

        connection.commit()
