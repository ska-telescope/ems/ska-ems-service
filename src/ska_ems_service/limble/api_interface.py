"""
This module interacts with Limble.
"""

# pylint: disable=R0801

import os

from ska_ems_service.utils.api_requests import get_request
from ska_ems_service.utils.exception_handler import (
    handle_request_exceptions,
    handle_response,
)

EMS_GATEWAY_TOKEN = os.getenv("EMS_GATEWAY_TOKEN")
EMS_GATEWAY_URL = os.getenv("EMS_GATEWAY_URL")
STFC_TOKEN = os.getenv("STFC_TOKEN")


@handle_request_exceptions
def get_regions():
    """
    Retrieve regions from Limble.

    Returns:
        dict: A JSON object containing the locations if successful
        , None otherwise.
    """
    api_url = f"{EMS_GATEWAY_URL}/limble/regions"
    headers = {
        "Authorization": f"Bearer {EMS_GATEWAY_TOKEN}",
        "Cookie": STFC_TOKEN,
    }
    params = {}

    request = get_request(api_url, headers, params)
    response = request.make_request()

    messages = {"success": "Successfully retrieved regions"}

    return handle_response(response, messages, return_json=True)


@handle_request_exceptions
def get_locations():
    """
    Retrieve locations from Limble.

    Returns:
        dict: A JSON object containing the locations if successful, None
        otherwise.
    """
    api_url = f"{EMS_GATEWAY_URL}/limble/locations"
    headers = {
        "Authorization": f"Bearer {EMS_GATEWAY_TOKEN}",
        "Cookie": STFC_TOKEN,
    }
    params = {}

    request = get_request(api_url, headers, params)
    response = request.make_request()

    messages = {"success": "Successfully retrieved locations"}
    return handle_response(response, messages, return_json=True)


@handle_request_exceptions
def get_assets(location_id):
    """
    Get assets from Limble throught the API.
    """
    # date_time = datetime.today() - timedelta(days=1)
    # unix_timestamp = time.mktime(date_time.timetuple())

    api_url = f"{EMS_GATEWAY_URL}/limble/assets"
    headers = {
        "Authorization": f"Bearer {EMS_GATEWAY_TOKEN}",
        "Cookie": STFC_TOKEN,
    }
    params = {"location_id": location_id}
    # params = {"locations": locationId, "start": unix_timestamp}

    request = get_request(api_url, headers, params)
    response = request.make_request()

    messages = {"success": "Successfully retrieved assets for location (%s)"}

    identifiers = {"location_id": location_id}

    return handle_response(
        response,
        messages,
        identifiers,
        return_json=True,
    )


@handle_request_exceptions
def get_asset_fields():
    """
    Retrieve asset fields for a given field name from Limble.

    Parameters:
        - field_name (str): The field name of the asset for which to retrieve
          fields.

    Returns:
        - dict: A dictionary containing the asset fields retrieved from the API
    """
    api_url = f"{EMS_GATEWAY_URL}/limble/asset_fields"
    headers = {
        "Authorization": f"Bearer {EMS_GATEWAY_TOKEN}",
        "Cookie": STFC_TOKEN,
    }
    params = {}

    request = get_request(api_url, headers, params)
    response = request.make_request()

    messages = {"success": "Successfully retrieved asset fields"}
    return handle_response(
        response,
        messages,
        return_json=True,
    )


@handle_request_exceptions
def get_tasks():
    """
    Get tasks from Limble throught the API.
    """
    api_url = f"{EMS_GATEWAY_URL}/limble/tasks"
    headers = {
        "Authorization": f"Bearer {EMS_GATEWAY_TOKEN}",
        "Cookie": STFC_TOKEN,
    }
    params = {}

    request = get_request(api_url, headers, params)
    response = request.make_request()

    messages = {"success": "Successfully retrieved tasks"}

    return handle_response(
        response,
        messages,
        return_json=True,
    )
