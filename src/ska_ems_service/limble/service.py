"""
This module manages maintenance management data synchronization between the
EMS database and EMS API Gateway.
"""

import logging
import uuid

import pandas as pd
from psycopg2.extras import execute_values

from ska_ems_service.database import create_ems_db_connection
from ska_ems_service.limble.api_interface import (
    get_asset_fields,
    get_assets,
    get_locations,
    get_regions,
    get_tasks,
)


def import_regions():
    """
    Import Limble regions into the EMS database by deleting existing
    regions and inserting new ones.
    """

    regions = get_regions()

    sql_insert_query = """
        INSERT INTO cmms_regions (id, name, parent_id)
        VALUES (%s, %s, %s);
    """

    with create_ems_db_connection() as connection:
        with connection.cursor() as cursor:
            cursor.execute("DELETE FROM cmms_regions")

            if regions:
                data = [
                    (
                        region["region_id"],
                        region["name"],
                        region["parent_region_id"],
                    )
                    for region in regions
                ]

                cursor.executemany(sql_insert_query, data)
                logging.info("Successfully imported %s regions", len(regions))
            else:
                logging.info("No regions found to import")

        connection.commit()


def import_locations():
    """
    Import Limble locations into the EMS database by deleting existing
    locations and inserting new ones.
    """

    locations = get_locations()

    sql_insert_query = """
        INSERT INTO cmms_locations (id, name, region_id)
        VALUES (%s, %s, %s);
    """

    with create_ems_db_connection() as connection:
        with connection.cursor() as cursor:
            cursor.execute("DELETE FROM cmms_locations")

            if locations:
                data = [
                    (
                        location["location_id"],
                        location["name"],
                        location["region_id"],
                    )
                    for location in locations
                ]

                cursor.executemany(sql_insert_query, data)
                logging.info(
                    "Successfully imported %s locations", len(locations)
                )
            else:
                logging.info("No locations found to import")

        connection.commit()


def import_assets():
    """
    Import Limble assets into the EMS database by deleting existing
    assets and inserting new ones.
    """

    locations = get_locations()

    if not locations:
        logging.warning("No locations found.")
        return

    # Collect asset data from all locations
    data = []
    for location in locations:
        location_id = location["location_id"]
        assets = get_assets(location_id)

        if assets:
            data.extend(
                (
                    asset["asset_id"],
                    asset["name"],
                    asset["parent_asset_id"],
                    asset["location_id"],
                    0,
                    "LIMBLE",
                    asset["asset_id"],
                    uuid.uuid4().hex,
                )
                for asset in assets
            )

    if not data:
        logging.warning("No assets to import.")
        return

    df = pd.DataFrame(
        data,
        columns=[
            "id",
            "name",
            "parent_id",
            "location_id",
            "type",
            "datasource",
            "datasource_id",
            "pk",
        ],
    )

    # Create mapping of asset_id to pk
    id_to_pk = dict(zip(df["id"], df["pk"]))
    df["parent_pk"] = df["parent_id"].map(id_to_pk)
    df["parent_pk"] = df["parent_pk"].where(pd.notna(df["parent_pk"]), None)

    # Save the data to the database
    with create_ems_db_connection() as connection:
        with connection.cursor() as cursor:
            cursor.execute("DELETE FROM cmms_assets WHERE datasource='LIMBLE'")

            # Convert DataFrame to list of tuples
            values = df.to_numpy().tolist()

            # Bulk insert into the temp table
            insert_query = """
                INSERT INTO cmms_assets
                (
                    id,
                    name,
                    parent_id,
                    location_id,
                    type,
                    datasource,
                    datasource_id,
                    pk,
                    parent_pk
                )
                VALUES %s
                ON CONFLICT (id) DO UPDATE
                SET
                    name = EXCLUDED.name,
                    parent_id = EXCLUDED.parent_id,
                    location_id = EXCLUDED.location_id,
                    type = EXCLUDED.type,
                    datasource = EXCLUDED.datasource,
                    datasource_id = EXCLUDED.datasource_id,
                    pk = EXCLUDED.pk,
                    parent_pk = EXCLUDED.parent_pk;
            """
            execute_values(cursor, insert_query, values)

            logging.info("Successfully imported %s assets", len(values))

        connection.commit()


def import_assets_fields():
    """
    Import Limble asset fields into the EMS database by updating existing
    assets with relevant asset fields.
    """

    asset_fields = {
        "Alim Part Number": "part_number",
        "Alim Version": "version",
        "Serial Number": "serial_number",
        "Topological Name": "topological_name",
    }

    field_values = get_asset_fields()
    if not field_values:
        logging.info("No asset field values found to import")
        return

    df = pd.DataFrame(field_values)
    df["column_name"] = df["field"].map(asset_fields)
    df = df.dropna(subset=["column_name"])
    df = df[
        ["asset_id", "column_name", "value"]
    ]  # Keep only necessary columns
    df_pivot = df.pivot_table(
        index="asset_id",
        columns="column_name",
        values="value",
        aggfunc="first",
    ).reset_index()

    if df_pivot.empty:
        logging.info("No data to update in cmms_assets")
        return

    # Replace NaN with None (so PostgreSQL will store it as NULL)
    df_pivot = df_pivot.where(pd.notna(df_pivot), None)

    with create_ems_db_connection() as connection:
        with connection.cursor() as cursor:
            # Create a temporary table
            cursor.execute(
                """
                CREATE TEMP TABLE temp_assets_update (
                    asset_id INTEGER PRIMARY KEY,
                    part_number TEXT,
                    version TEXT,
                    serial_number TEXT,
                    topological_name TEXT
                ) ON COMMIT DROP;
            """
            )

            # Convert DataFrame to list of tuples
            values = df_pivot.to_numpy().tolist()

            # Bulk insert into the temp table
            insert_query = """
                INSERT INTO temp_assets_update
                (
                    asset_id,
                    part_number,
                    serial_number,
                    topological_name,
                    version
                )
                VALUES %s
                ON CONFLICT (asset_id) DO UPDATE
                SET
                    part_number = EXCLUDED.part_number,
                    serial_number = EXCLUDED.serial_number,
                    topological_name = EXCLUDED.topological_name,
                    version = EXCLUDED.version;
            """
            execute_values(cursor, insert_query, values)

            # Perform a single update using JOIN
            cursor.execute(
                """
                UPDATE cmms_assets AS ca
                SET
                    part_number = t.part_number,
                    version = t.version,
                    serial_number = t.serial_number,
                    topological_name = t.topological_name
                FROM temp_assets_update AS t
                WHERE ca.id = t.asset_id;
            """
            )

            logging.info("Successfully updated %s assets", cursor.rowcount)

        connection.commit()


def import_tasks():
    """
    Import Limble tasks into the EMS database by deleting existing
    tasks and inserting new ones.
    """
    tasks = get_tasks()

    sql_insert_query = """
        INSERT INTO cmms_tasks (name, description, created_date, start_date,
        due_date, completed_date, last_edited, priority, location_id, asset_id,
        status, type, assigned_to, datasource, datasource_id)
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);
    """

    with create_ems_db_connection() as connection:
        with connection.cursor() as cursor:
            cursor.execute("DELETE FROM cmms_tasks WHERE datasource='LIMBLE'")

            if tasks:
                data = [
                    (
                        task["name"],
                        task["description"],
                        task["created_date"],
                        task["start_date"],
                        task["due_date"],
                        task["completed_date"],
                        task["last_edited_date"],
                        task["priority"],
                        task["location_id"],
                        task["asset_id"],
                        task["status"],
                        task["type"],
                        None,
                        "LIMBLE",
                        task["task_id"],
                    )
                    for task in tasks
                ]

                cursor.executemany(sql_insert_query, data)
                logging.info("Successfully imported %s tasks", len(tasks))
            else:
                logging.info("No tasks found to import")

        connection.commit()
