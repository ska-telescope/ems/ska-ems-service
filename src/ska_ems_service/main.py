"""Simple FASTAPI App for workshop purposes

Returns:
  None
"""

import os

from apscheduler.schedulers.background import BackgroundScheduler
from fastapi import FastAPI

from ska_ems_service import config  # noqa: F401  # pylint: disable=W0611
from ska_ems_service.alim import service as alim_services
from ska_ems_service.csdb import service as csdb_services
from ska_ems_service.eda import service as eda_services
from ska_ems_service.jira import service as jira_services
from ska_ems_service.limble import service as limble_services
from ska_ems_service.ramlog import service as ramlog_services

app = FastAPI(
    title="Engineering Management System (EMS) Scheduler",
    summary="""
    This API is used to control the Engineering
    Management System (EMS) Task Scheduler.
    """,
)

scheduler = BackgroundScheduler()


def update_ems_data():
    """Update EMS data"""
    limble_services.import_regions()
    limble_services.import_locations()
    limble_services.import_assets()
    limble_services.import_assets_fields()
    limble_services.import_tasks()

    # alim_services.import_product_structure()

    csdb_services.import_data_modules()

    jira_services.import_problem_reports()

    # eda_services.import_eda_values()

    # ramlog_services.import_product()


scheduler.add_job(update_ems_data, "interval", minutes=30)


@app.get("/")
async def root():
    """Unit test for the root path "/" """
    return {"message": "Hello World"}


@app.get("/test")
async def test():
    """Unit test for the root path "/" """
    db = os.getenv("EMS_GATEWAY_TOKEN", default="No Env")
    return {"message": db}


@app.get("/start")
async def start():
    """Start scheduler"""
    scheduler.start()
    return {"message": "Service Started"}


@app.get("/stop")
async def stop():
    """Unit test for the root path "/" """
    scheduler.shutdown()
    return {"message": "Service Stoped"}


@app.get("/limble")
async def update_limble():
    """Update Limble data"""

    limble_services.import_regions()
    limble_services.import_locations()

    limble_services.import_assets()
    limble_services.import_assets_fields()

    limble_services.import_tasks()

    return {"message": "Limble data updated"}


@app.get("/alim")
async def update_alim():
    """Update Alim data"""

    alim_services.import_product_structure()

    return {"message": "Alim data updated"}


@app.get("/data-modules")
async def update_data_modules():
    """Update data modules"""

    csdb_services.import_data_modules()

    return {"message": "Data modules updated"}


@app.get("/sprts")
async def update_sprts():
    """Update SPRTS data"""

    jira_services.import_problem_reports()

    return {"message": "SPRTS data updated"}


@app.get("/eda")
async def update_eda():
    """Update EDA data"""

    eda_services.import_eda_values()

    return {"message": "EDA data updated"}


@app.get("/ramlog")
async def update_ramlog():
    """Update RamLog data"""

    ramlog_services.import_lsa_product_breakdown_structures()
    # ramlog_services.import_fde_product_breakdown_structures()

    return {"message": "RamLog data updated"}
