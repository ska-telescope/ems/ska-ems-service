"""
This module interacts with JIRA.
"""

# pylint: disable=R0801
import os

from ska_ems_service.utils.api_requests import post_request
from ska_ems_service.utils.exception_handler import (
    handle_request_exceptions,
    handle_response,
)

EMS_GATEWAY_TOKEN = os.getenv("EMS_GATEWAY_TOKEN")
# EMS_GATEWAY_URL = os.getenv("EMS_GATEWAY_URL")
EMS_GATEWAY_URL = "https://ems-api.internal.skao.int"


@handle_request_exceptions
def get_eda_values(device_urls):
    """
    Retrieve the EDA values from the EMS API Gateway.
    """
    api_url = f"{EMS_GATEWAY_URL}/eda/value"
    headers = {"Authorization": f"Bearer {EMS_GATEWAY_TOKEN}"}
    payload = device_urls

    request = post_request(api_url, headers, payload)
    response = request.make_request()

    messages = {"success": "Successfully retrieved values"}

    return handle_response(
        response,
        messages,
        return_json=True,
    )
