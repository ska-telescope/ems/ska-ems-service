"""
This module manages problem reporting data synchronization between the EMS
database and EMS API Gateway.
"""

import csv
import logging
import os

from ska_ems_service.database import create_ems_db_connection
from ska_ems_service.eda.api_interface import get_eda_values


def load_mapping_from_csv():
    """
    Load mapping from a CSV file located in the same directory as this script.
    """
    file_path = os.path.join(os.path.dirname(__file__), "mapping.csv")
    mapping = {}
    with open(file_path, mode="r", encoding="utf-8") as file:
        reader = csv.DictReader(file)
        for row in reader:
            mapping[row["topological_name"]] = str(row["tango_url"])
    return mapping


def import_eda_values():
    """
    Import EDA data into the EMS database.
    """
    mapping = load_mapping_from_csv()

    device_urls = list(mapping.values())
    values = get_eda_values(device_urls)

    url_to_topo = {url: topo_name for topo_name, url in mapping.items()}

    sql_update_query = """
        UPDATE cmms_assets
        SET status = %s
        WHERE topological_name = %s
    """

    with create_ems_db_connection() as connection:
        with connection.cursor() as cursor:
            for entry in values:
                tango_url = entry["name"]
                status_value = entry["value"]
                topological_name = url_to_topo.get(tango_url)

                if topological_name:
                    try:
                        cursor.execute(
                            sql_update_query, (status_value, topological_name)
                        )
                        logging.info(
                            "Updated %s with status '%s'",
                            topological_name,
                            status_value,
                        )
                    except Exception as e:  # pylint: disable=broad-except
                        logging.error(
                            "Failed to update %s: %s", topological_name, e
                        )

        connection.commit()
        logging.info("Successfully imported eda values")
