"""
This module manages problem reporting data synchronization between the EMS
database and EMS API Gateway.
"""

import logging

from ska_ems_service.database import create_ems_db_connection
from ska_ems_service.jira.api_interface import get_prts_tickets


def import_problem_reports():
    """
    Import problem report data into the EMS database.
    """
    problem_reports = get_prts_tickets()

    sql_insert_query = """
        INSERT INTO prts_issues (id, jira_key, summary, description
        , status, resolution, created_date, resolution_date, url)
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s);
    """
    sql_insert_component_query = """
        INSERT INTO prts_issue_components (issue_id, name, part_number)
        VALUES (%s, %s, %s);
        """

    with create_ems_db_connection() as connection:
        with connection.cursor() as cursor:
            cursor.execute("DELETE FROM prts_issue_components")
            cursor.execute("DELETE FROM prts_issues")

            if problem_reports:
                data = [
                    (
                        problem_report["id"],
                        problem_report["key"],
                        problem_report["summary"],
                        problem_report["description"],
                        problem_report["status"],
                        problem_report["resolution"],
                        problem_report["created_date"],
                        problem_report["resolution_date"],
                        problem_report["url"],
                    )
                    for problem_report in problem_reports
                ]
                cursor.executemany(sql_insert_query, data)

                # Insert components for each problem report
                for problem_report in problem_reports:
                    jira_id = problem_report["id"]
                    for component in problem_report.get("components", []):
                        if "name" in component:
                            data = (
                                jira_id,
                                component["name"],
                                component["part_number"],
                            )
                            cursor.execute(sql_insert_component_query, data)

                logging.info(
                    "Successfully imported %s problem reports",
                    len(problem_reports),
                )
            else:
                logging.info("No problem reports found to import")

        connection.commit()
