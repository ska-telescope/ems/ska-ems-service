"""
Simple FASTAPI App for workshop purposes

Returns:
  None
"""

import warnings

from fastapi.testclient import TestClient

from ska_ems_service.main import app

client = TestClient(app)


def api():
    """API version warning"""
    warnings.warn(UserWarning("no api version is specified!"))
    return 1


def test_read_main():
    """Unit test for the root path "/" """
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"message": "Hello World"}
    assert api() == 1
